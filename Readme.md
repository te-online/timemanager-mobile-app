# TimeManager Mobile App

A mobile app to track time and sync with the [Nextcloud TimeManager app](https://apps.nextcloud.com/apps/timemanager).

**Note:** The main focus of this project is on developing a companion app for **iOS devices**. Android should be supported, but support is not actively tested. If you're looking for a companion app for Android, try the [Android app (currently in beta)](https://play.google.com/store/apps/details?id=de.jbservices.nc_timemanager_app) made by [@joleaf](https://gitlab.com/joleaf/nc-timemanager-mobile-app).

## Features

@TODO:

## Planned features

@TODO:

## Development

This app uses [`expo`](https://expo.dev) and `react-native` and is developed in TypeScript. It is built in components and styling is applied using `styled-components`.

Before building or development, dependencies need to be installed once by running `yarn`. A recent version of yarn is included in the repository files, but you might need to install a global version of yarn on your machine using `npm i -g yarn` to make things work.

To start expo run `yarn start` or `expo start`. Please refer to [expo's documentation](https://docs.expo.dev/) for further guidance on how to start the app on local emulators/simulators or physical devices and on how to develop using expo.

## Contributing

Every form of contribution is welcome!

Remember to use a friendly tone and to follow [Nextcloud's code of conduct](https://nextcloud.com/contribute/code-of-conduct/).

Please [open issues](https://gitlab.com/te-online/timemanager-mobile-app/-/issues/new?issue) if you experience bugs or have feature requests.

If you're working on a specific feature or bugfix, please create a fork of the project and open a new [Merge Request](https://gitlab.com/te-online/timemanager-mobile-app/-/merge_requests/new) even before you write the first line of code, so it is obvious what you're working and questions from your side can be answered as well as feedback be provided.

## Changelog

### 0.1.0 Beta, tba

- Initial release.
- Featureset:
  - tba
