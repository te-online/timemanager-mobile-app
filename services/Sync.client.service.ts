import Realm from "realm";
import { Client } from "../schemas/Client.schema";
import { Commit } from "../schemas/Commit.schema";
import { Project } from "../schemas/Project.schema";
import { Task } from "../schemas/Task.schema";
import { Time } from "../schemas/Time.schema";

const databaseOptions: Realm.Configuration = {
  path: "local.realm",
  schema: [
    Commit.schema,
    Client.schema,
    Project.schema,
    Task.schema,
    Time.schema,
  ],
  schemaVersion: 1,
  deleteRealmIfMigrationNeeded: false,
};

export class SyncClientService {
  public async execute(): Promise<void> {}

  public static async testWrite() {
    const realm = await Realm.open(databaseOptions);
    return await realm.write(async () => {
      await realm.create<Commit>("Commit", {
        commit: new Realm.BSON.UUID(),
        created: new Date(),
      });
    });
  }

  public static async testRead() {
    const realm = await Realm.open(databaseOptions);
    return realm.objects<Commit>("Commit");
  }
}
