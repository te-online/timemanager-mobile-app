import { Client } from "./Client.schema";
import { GenericObject } from "../types/object.types";

export class Project extends GenericObject {
  public name?: string;
  public note?: string;
  public color?: string;
  public client_uuid: string = "";
  public client?: Realm.Results<Client>;

  public static schema: Realm.ObjectSchema = {
    name: "Project",
    primaryKey: "uuid",
    properties: {
      uuid: "uuid",
      updated: "date",
      created: "date",
      commit: "uuid",
      name: "string?",
      note: "string?",
      color: "string?",
      client_uuid: "string",
      client: {
        type: "linkingObjects",
        objectType: "Client",
        property: "projects",
      },
      tasks: "Task[]",
    } as Record<keyof Project, any>,
  };
}
