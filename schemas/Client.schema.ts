import { GenericObject } from "../types/object.types";

export class Client extends GenericObject {
  public name?: string;
  public note?: string;
  public email?: string;
  public phone?: string;
  public street?: string;
  public postcode?: string;
  public city?: string;
  public web?: string;

  public static schema: Realm.ObjectSchema = {
    name: "Client",
    primaryKey: "uuid",
    properties: {
      uuid: "uuid",
      updated: "date",
      created: "date",
      commit: "uuid",
      name: "string?",
      note: "string?",
      email: "string?",
      phone: "string?",
      street: "string?",
      postcode: "string?",
      city: "string?",
      web: "string?",
      projects: "Project[]",
    } as Record<keyof Client, any>,
  };
}
