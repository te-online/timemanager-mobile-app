import Realm from 'realm'
export class Commit {
  public commit: Realm.BSON.UUID = new Realm.BSON.UUID();
  public created: Date = new Date();

  public static schema: Realm.ObjectSchema = {
    name: "Commit",
    primaryKey: "commit",
    properties: {
      commit: "uuid",
      created: "date",
    } as Record<keyof Commit, any>,
  };
}
