import { GenericObject } from "../types/object.types";
import { Project } from "./Project.schema";

export class Task extends GenericObject {
  public name?: string;
  public project_uuid: string = "";
  public project?: Realm.Results<Project>;

  public static schema: Realm.ObjectSchema = {
    name: "Task",
    primaryKey: "uuid",
    properties: {
      uuid: "uuid",
      updated: "date",
      created: "date",
      commit: "uuid",
      name: "string?",
      project_uuid: "string",
      project: {
        type: "linkingObjects",
        objectType: "Project",
        property: "tasks",
      },
			times: "Time[]",
    } as Record<keyof Task, any>,
  };
}
