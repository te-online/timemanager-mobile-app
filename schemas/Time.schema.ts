import { GenericObject } from "../types/object.types";
import { Task } from "./Task.schema";

export class Time extends GenericObject {
  public start: Date = new Date();
  public end: Date = new Date();
  public note?: string;
  public paymentStatus?: string;
  public task_uuid: string = "";
  public task?: Realm.Results<Task>;

  public static schema: Realm.ObjectSchema = {
    name: "Time",
    primaryKey: "uuid",
    properties: {
      uuid: "uuid",
      updated: "date",
      created: "date",
      commit: "uuid",
      start: "date",
      end: "date",
      note: "string?",
      paymentStatus: "string?",
      task_uuid: "string",
      task: {
        type: "linkingObjects",
        objectType: "Task",
        property: "times",
      },
    } as Record<keyof Time, any>,
  };
}
