import { StatusBar } from "expo-status-bar";
import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SyncClientService } from "./services/Sync.client.service";

export default function App() {
  useEffect(() => {
    (async () => {
      console.log("writing");
      await SyncClientService.testWrite();

      console.log("read", await SyncClientService.testRead());
    })();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Open up App.tsx to start working on your app!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
