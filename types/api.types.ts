import { Client } from "../schemas/Client.schema";
import { Commit } from "../schemas/Commit.schema";
import { Project } from "../schemas/Project.schema";
import { Task } from "../schemas/Task.schema";
import { Time } from "../schemas/Time.schema";

export interface ChangeSet<T> {
  created: T[];
  updated: T[];
  deleted: T[];
}

export interface SyncRequest {
  lastCommit: Commit["commit"];
  data: {
    clients: ChangeSet<Client>;
    projects: ChangeSet<Project>;
    tasks: ChangeSet<Task>;
    times: ChangeSet<Time>;
  };
}

export interface SyncResponse extends SyncRequest {}
