export class GenericObject {
  public uuid: Realm.BSON.UUID = new Realm.BSON.UUID();
  public created: Date = new Date();
  public updated: Date = new Date();
  public commit: Realm.BSON.UUID = new Realm.BSON.UUID();
}